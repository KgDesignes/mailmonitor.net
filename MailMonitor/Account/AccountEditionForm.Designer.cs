﻿namespace MailMonitor
{
    partial class AccountEditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAccept = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudCheckInterval = new System.Windows.Forms.NumericUpDown();
            this.lblCheckInterval = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblHost = new System.Windows.Forms.Label();
            this.tbHost = new System.Windows.Forms.TextBox();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.lblPort = new System.Windows.Forms.Label();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.browseProgramOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.cbSSL = new System.Windows.Forms.CheckBox();
            this.buttonTestCommand = new System.Windows.Forms.Button();
            this.gbAccountName = new System.Windows.Forms.GroupBox();
            this.gbServerInfo = new System.Windows.Forms.GroupBox();
            this.gbCommand = new System.Windows.Forms.GroupBox();
            this.lblCommandArgs = new System.Windows.Forms.Label();
            this.tbCommandArgs = new System.Windows.Forms.TextBox();
            this.lblCommand = new System.Windows.Forms.Label();
            this.tbCommand = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCheckInterval)).BeginInit();
            this.gbAccountName.SuspendLayout();
            this.gbServerInfo.SuspendLayout();
            this.gbCommand.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAccept
            // 
            this.buttonAccept.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonAccept.Location = new System.Drawing.Point(230, 302);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(120, 23);
            this.buttonAccept.TabIndex = 4;
            this.buttonAccept.Text = "&Accept";
            this.buttonAccept.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(356, 302);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(120, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudCheckInterval);
            this.groupBox1.Controls.Add(this.lblCheckInterval);
            this.groupBox1.Controls.Add(this.lblPassword);
            this.groupBox1.Controls.Add(this.tbPassword);
            this.groupBox1.Controls.Add(this.lblUsername);
            this.groupBox1.Controls.Add(this.tbUsername);
            this.groupBox1.Location = new System.Drawing.Point(12, 114);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(464, 102);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Account Information";
            // 
            // nudCheckInterval
            // 
            this.nudCheckInterval.Location = new System.Drawing.Point(112, 71);
            this.nudCheckInterval.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.nudCheckInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCheckInterval.Name = "nudCheckInterval";
            this.nudCheckInterval.Size = new System.Drawing.Size(75, 20);
            this.nudCheckInterval.TabIndex = 5;
            this.nudCheckInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblCheckInterval
            // 
            this.lblCheckInterval.Location = new System.Drawing.Point(6, 73);
            this.lblCheckInterval.Name = "lblCheckInterval";
            this.lblCheckInterval.Size = new System.Drawing.Size(100, 13);
            this.lblCheckInterval.TabIndex = 4;
            this.lblCheckInterval.Text = "Check Interval";
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(6, 48);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(100, 13);
            this.lblPassword.TabIndex = 2;
            this.lblPassword.Text = "Password";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(112, 45);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(346, 20);
            this.tbPassword.TabIndex = 3;
            this.tbPassword.UseSystemPasswordChar = true;
            // 
            // lblUsername
            // 
            this.lblUsername.Location = new System.Drawing.Point(6, 22);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(100, 13);
            this.lblUsername.TabIndex = 0;
            this.lblUsername.Text = "Username";
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(112, 19);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(346, 20);
            this.tbUsername.TabIndex = 1;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(6, 19);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(452, 20);
            this.tbName.TabIndex = 0;
            // 
            // lblHost
            // 
            this.lblHost.Location = new System.Drawing.Point(6, 22);
            this.lblHost.Name = "lblHost";
            this.lblHost.Size = new System.Drawing.Size(64, 13);
            this.lblHost.TabIndex = 0;
            this.lblHost.Text = "Host";
            // 
            // tbHost
            // 
            this.tbHost.Location = new System.Drawing.Point(76, 19);
            this.tbHost.Name = "tbHost";
            this.tbHost.Size = new System.Drawing.Size(199, 20);
            this.tbHost.TabIndex = 1;
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(323, 19);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(49, 20);
            this.tbPort.TabIndex = 3;
            // 
            // lblPort
            // 
            this.lblPort.Location = new System.Drawing.Point(281, 22);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(36, 13);
            this.lblPort.TabIndex = 2;
            this.lblPort.Text = "Port";
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(358, 17);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(100, 23);
            this.buttonBrowse.TabIndex = 4;
            this.buttonBrowse.Text = "&Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // browseProgramOpenFileDialog
            // 
            this.browseProgramOpenFileDialog.FileName = "Select a program";
            // 
            // cbSSL
            // 
            this.cbSSL.Location = new System.Drawing.Point(378, 17);
            this.cbSSL.Name = "cbSSL";
            this.cbSSL.Size = new System.Drawing.Size(80, 24);
            this.cbSSL.TabIndex = 4;
            this.cbSSL.Text = "Use SSL";
            this.cbSSL.UseVisualStyleBackColor = true;
            // 
            // buttonTestCommand
            // 
            this.buttonTestCommand.Location = new System.Drawing.Point(358, 43);
            this.buttonTestCommand.Name = "buttonTestCommand";
            this.buttonTestCommand.Size = new System.Drawing.Size(100, 23);
            this.buttonTestCommand.TabIndex = 5;
            this.buttonTestCommand.Text = "&Test";
            this.buttonTestCommand.UseVisualStyleBackColor = true;
            this.buttonTestCommand.Click += new System.EventHandler(this.buttonTestCommand_Click);
            // 
            // gbAccountName
            // 
            this.gbAccountName.Controls.Add(this.tbName);
            this.gbAccountName.Location = new System.Drawing.Point(12, 12);
            this.gbAccountName.Name = "gbAccountName";
            this.gbAccountName.Size = new System.Drawing.Size(464, 45);
            this.gbAccountName.TabIndex = 0;
            this.gbAccountName.TabStop = false;
            this.gbAccountName.Text = "Account Name";
            // 
            // gbServerInfo
            // 
            this.gbServerInfo.Controls.Add(this.lblHost);
            this.gbServerInfo.Controls.Add(this.cbSSL);
            this.gbServerInfo.Controls.Add(this.tbHost);
            this.gbServerInfo.Controls.Add(this.tbPort);
            this.gbServerInfo.Controls.Add(this.lblPort);
            this.gbServerInfo.Location = new System.Drawing.Point(12, 63);
            this.gbServerInfo.Name = "gbServerInfo";
            this.gbServerInfo.Size = new System.Drawing.Size(464, 45);
            this.gbServerInfo.TabIndex = 1;
            this.gbServerInfo.TabStop = false;
            this.gbServerInfo.Text = "Server Information";
            // 
            // gbCommand
            // 
            this.gbCommand.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.gbCommand.Controls.Add(this.lblCommandArgs);
            this.gbCommand.Controls.Add(this.tbCommandArgs);
            this.gbCommand.Controls.Add(this.buttonTestCommand);
            this.gbCommand.Controls.Add(this.lblCommand);
            this.gbCommand.Controls.Add(this.buttonBrowse);
            this.gbCommand.Controls.Add(this.tbCommand);
            this.gbCommand.Location = new System.Drawing.Point(12, 222);
            this.gbCommand.Name = "gbCommand";
            this.gbCommand.Size = new System.Drawing.Size(464, 74);
            this.gbCommand.TabIndex = 3;
            this.gbCommand.TabStop = false;
            this.gbCommand.Text = "Command";
            // 
            // lblCommandArgs
            // 
            this.lblCommandArgs.Location = new System.Drawing.Point(6, 48);
            this.lblCommandArgs.Name = "lblCommandArgs";
            this.lblCommandArgs.Size = new System.Drawing.Size(100, 13);
            this.lblCommandArgs.TabIndex = 2;
            this.lblCommandArgs.Text = "Arguments";
            // 
            // tbCommandArgs
            // 
            this.tbCommandArgs.Location = new System.Drawing.Point(112, 45);
            this.tbCommandArgs.Name = "tbCommandArgs";
            this.tbCommandArgs.Size = new System.Drawing.Size(240, 20);
            this.tbCommandArgs.TabIndex = 3;
            // 
            // lblCommand
            // 
            this.lblCommand.Location = new System.Drawing.Point(6, 22);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(100, 13);
            this.lblCommand.TabIndex = 0;
            this.lblCommand.Text = "Program / URL";
            // 
            // tbCommand
            // 
            this.tbCommand.Location = new System.Drawing.Point(112, 19);
            this.tbCommand.Name = "tbCommand";
            this.tbCommand.Size = new System.Drawing.Size(240, 20);
            this.tbCommand.TabIndex = 1;
            // 
            // AccountEditionForm
            // 
            this.AcceptButton = this.buttonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(488, 336);
            this.ControlBox = false;
            this.Controls.Add(this.gbCommand);
            this.Controls.Add(this.gbServerInfo);
            this.Controls.Add(this.gbAccountName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AccountEditionForm";
            this.ShowInTaskbar = false;
            this.Text = "Account Edit";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCheckInterval)).EndInit();
            this.gbAccountName.ResumeLayout(false);
            this.gbAccountName.PerformLayout();
            this.gbServerInfo.ResumeLayout(false);
            this.gbServerInfo.PerformLayout();
            this.gbCommand.ResumeLayout(false);
            this.gbCommand.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAccept;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Label lblHost;
        private System.Windows.Forms.TextBox tbHost;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.OpenFileDialog browseProgramOpenFileDialog;
        private System.Windows.Forms.CheckBox cbSSL;
        private System.Windows.Forms.Button buttonTestCommand;
        private System.Windows.Forms.GroupBox gbAccountName;
        private System.Windows.Forms.GroupBox gbServerInfo;
        private System.Windows.Forms.GroupBox gbCommand;
        private System.Windows.Forms.Label lblCommandArgs;
        private System.Windows.Forms.TextBox tbCommandArgs;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.TextBox tbCommand;
        private System.Windows.Forms.NumericUpDown nudCheckInterval;
        private System.Windows.Forms.Label lblCheckInterval;
    }
}