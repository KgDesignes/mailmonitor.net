﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailMonitor
{
    class AccountEventArgs
    {
        private Guid uid;
        public Guid UID
        {
            get { return this.uid; }
        }

        private Account account;
        public Account Account
        {
            get { return this.account; }
        }

        public AccountEventArgs(Guid uid, Account account)
        {
            this.uid = uid;
            this.account = account;
        }
    }
}
