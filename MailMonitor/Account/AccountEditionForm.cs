﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace MailMonitor
{
    public partial class AccountEditionForm : Form
    {
        private Account account;

        public AccountEditionForm(Account account)
        {
            InitializeComponent();

            this.account = account;

            this.tbName.Text = this.account.Name;
            this.tbHost.Text = this.account.Host;
            this.tbUsername.Text = this.account.Username;
            this.tbPassword.Text = this.account.Password;
            this.tbPort.Text = this.account.Port > 0 ? this.account.Port.ToString() : "";
            this.cbSSL.Checked = this.account.Secure;
            this.nudCheckInterval.Value = this.account.CheckInterval;
            this.tbCommand.Text = this.account.Command;
            this.tbCommandArgs.Text = this.account.CommandArgs;
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = browseProgramOpenFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                tbCommand.Text = browseProgramOpenFileDialog.FileName;
            }
        }

        internal void SaveAccount()
        {
            this.account.Name = this.tbName.Text;
            this.account.Host = this.tbHost.Text;
            this.account.Username = this.tbUsername.Text;
            this.account.Password = this.tbPassword.Text;
            int port = -1;

            int.TryParse(this.tbPort.Text, out port);
            this.account.Port = (port > 0) ? port : this.account.Port;
            this.account.Secure = this.cbSSL.Checked;
            this.account.CheckInterval = Convert.ToInt32(this.nudCheckInterval.Value);
            this.account.Command = this.tbCommand.Text;
            this.account.CommandArgs = this.tbCommandArgs.Text;
        }

        private void buttonTestCommand_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(this.tbCommand.Text, this.tbCommandArgs.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ejecutando el comando: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
