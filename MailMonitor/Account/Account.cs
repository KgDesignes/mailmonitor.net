﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Xml;

namespace MailMonitor
{
    public class Account
    {
        public Account()
        {
            this.uid = Guid.NewGuid();
        }

        private Guid uid;

        public Guid UID
        {
            get { return uid; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string host;

        public string Host
        {
            get { return host; }
            set { host = value; }
        }
        private string username;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        private int port;

        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        private bool secure;

        public bool Secure
        {
            get { return secure; }
            set { secure = value; }
        }

        private int checkInterval;

        public int CheckInterval
        {
            get { return checkInterval > 1 ? checkInterval : 5; }
            set { checkInterval = value; }
        }

        private string command;

        public string Command
        {
            get { return command; }
            set { command = value; }
        }

        private string commandArgs;

        public string CommandArgs
        {
            get { return commandArgs; }
            set { commandArgs = value; }
        }

        public XmlDocument GetXML()
        {
            XmlDocument doc = new XmlDocument();

            XmlElement rootElement = doc.CreateElement("Account");

            XmlElement tmpElement;

            tmpElement = doc.CreateElement("Name");
            tmpElement.AppendChild( doc.CreateTextNode(this.Name) );

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("Host");
            tmpElement.AppendChild(doc.CreateTextNode(this.Host));

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("Username");
            tmpElement.AppendChild(doc.CreateTextNode(this.Username));

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("Password");
            tmpElement.AppendChild( doc.CreateTextNode(this.Password ) );

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("Port");
            tmpElement.AppendChild(doc.CreateTextNode(this.Port.ToString()));

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("Secure");
            tmpElement.AppendChild( doc.CreateTextNode(this.Secure ? "1" : "0"));

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("CheckInterval");
            tmpElement.AppendChild(doc.CreateTextNode(this.CheckInterval.ToString()));

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("Command");
            tmpElement.AppendChild(doc.CreateTextNode(this.Command));

            rootElement.AppendChild(tmpElement);

            tmpElement = doc.CreateElement("CommandArgs");
            tmpElement.AppendChild(doc.CreateTextNode(this.CommandArgs));

            rootElement.AppendChild(tmpElement);

            doc.AppendChild(rootElement);
            
            return doc;
        }

        public void SetXml(XmlDocument doc)
        {
            XmlElement element = doc.DocumentElement;
            XmlNodeList tmpNode = element.ChildNodes;

            tmpNode = element.GetElementsByTagName("Name");
            this.name = tmpNode.Count > 0 ? tmpNode[0].InnerText : "";

            tmpNode = element.GetElementsByTagName("Host");
            this.host = tmpNode.Count > 0 ? tmpNode[0].InnerText : "";

            tmpNode = element.GetElementsByTagName("Username");
            this.username = tmpNode.Count > 0 ? tmpNode[0].InnerText : "";

            tmpNode = element.GetElementsByTagName("Password");
            this.password = tmpNode.Count > 0 ? tmpNode[0].InnerText : "";

            tmpNode = element.GetElementsByTagName("Port");
            int.TryParse(tmpNode.Count > 0 ? tmpNode[0].InnerText : "", out this.port);

            tmpNode = element.GetElementsByTagName("Secure");
            this.secure = ((tmpNode.Count > 0 ? tmpNode[0].InnerText : "0") == "1");

            tmpNode = element.GetElementsByTagName("CheckInterval");
            int.TryParse(tmpNode.Count > 0 ? tmpNode[0].InnerText : "", out this.checkInterval);

            tmpNode = element.GetElementsByTagName("Command");
            this.command = tmpNode.Count > 0 ? tmpNode[0].InnerText : "";

            tmpNode = element.GetElementsByTagName("CommandArgs");
            this.commandArgs = tmpNode.Count > 0 ? tmpNode[0].InnerText : "";
        }
    }
}
