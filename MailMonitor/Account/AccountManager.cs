﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Xml;
using System.Windows.Forms;

using MailMonitor.Security;

namespace MailMonitor
{
    class AccountManager
    {
        private string masterPassword;

        public string MasterPassword
        {
            get { return masterPassword; }
        }

        private Dictionary<Guid,Account> accounts;

        public Dictionary<Guid, Account> Accounts
        {
            get { return accounts; }
        }

        private EncryptionHelper encryptionHelper;

        public delegate void AccountChangeHandler(object sender, AccountEventArgs args);

        public event AccountChangeHandler AccountAdded;
        public event AccountChangeHandler AccountUpdated;
        public event AccountChangeHandler AccountDeleted;

        public AccountManager()
        {
        }

        public bool LoadSettings()
        {
            this.masterPassword = Properties.Settings.Default.MasterPassword;

            if (!String.IsNullOrEmpty(Properties.Settings.Default.MasterPassword))
            {
                MasterPasswordInputForm masterPasswordInputForm = new MasterPasswordInputForm();

                DialogResult ddr = masterPasswordInputForm.ShowDialog();

                string masterPassword = masterPasswordInputForm.Password;

                if ( this.masterPassword != EncryptionHelper.getHash(masterPassword))
                {
                    return false;
                }

                this.encryptionHelper = new EncryptionHelper(masterPassword);
            }
            else
            {
                this.encryptionHelper = new EncryptionHelper("");
            }

            this.accounts = new Dictionary<Guid, Account>();
            StringCollection accountStrings = Properties.Settings.Default.Accounts;

            if (accountStrings != null)
            {
                for (int i = 0; i < accountStrings.Count; i += 1)
                {
                    string xmlText = this.encryptionHelper.Decrypt(accountStrings[i]);

                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.LoadXml(xmlText);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error cargando XML de cuenta: " + ex.Message, "MailMonitor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        continue;
                    }

                    Account tmpAccount = new Account();
                    tmpAccount.SetXml(doc);

                    this.accounts.Add(tmpAccount.UID, tmpAccount);
                }
            }

            return true;
        }

        public void SaveSettings() {
            Properties.Settings.Default.MasterPassword = this.masterPassword;

            StringCollection accountStrings = new StringCollection();
            
            foreach ( Guid uid in this.accounts.Keys ) {
                XmlDocument accountXml = this.accounts[uid].GetXML();
                string accountData = this.encryptionHelper.Encrypt(accountXml.OuterXml);
                accountStrings.Add(accountData);
            }

            Properties.Settings.Default.Accounts = accountStrings;
            Properties.Settings.Default.Save();
        }

        internal void AddAccount()
        {
            Account tmpAccount = new Account();
            AccountEditionForm form = new AccountEditionForm(tmpAccount);
            DialogResult result = form.ShowDialog();

            if (result == DialogResult.OK)
            {
                form.SaveAccount();
                this.accounts.Add(tmpAccount.UID, tmpAccount);
                this.SaveSettings();
                if (this.AccountAdded != null)
                {
                    this.AccountAdded(this, new AccountEventArgs(tmpAccount.UID, tmpAccount));
                }
            }
        }

        internal void EditAccount(Guid uid)
        {
            Account account = this.accounts.ContainsKey(uid) ? this.accounts[uid] : null;

            if (account == null)
            {
                return;
            }

            AccountEditionForm form = new AccountEditionForm(account);
            DialogResult result = form.ShowDialog();

            if (result == DialogResult.OK)
            {
                form.SaveAccount();
                this.accounts[uid] = account;
                this.SaveSettings();
                if (this.AccountUpdated != null)
                {
                    this.AccountUpdated(this, new AccountEventArgs(uid, account));
                }
            }
        }

        internal void DeleteAccount(Guid uid)
        {
            if (this.accounts.ContainsKey(uid))
            {
                Account account = this.accounts[uid];
                this.accounts.Remove(uid);
                if (this.AccountDeleted != null)
                {
                    this.AccountDeleted(this, new AccountEventArgs(uid, account));
                }
            }
        }
    }
}
