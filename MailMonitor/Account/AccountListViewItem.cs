﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MailMonitor
{
    class AccountListViewItem:ListViewItem
    {
        private Guid uid;

        public Guid UID
        {
            get { return uid; }
        }

        private Account account;

        public Account Account
        {
            get { return account; }
        }

        public AccountListViewItem(Guid uid, Account account)
        {
            this.uid = uid;
            this.account = account;

            this.Text = account.Name;
            this.Name = uid.ToString();
        }
    }
}
