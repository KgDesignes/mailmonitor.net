﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MailMonitor.Popup
{
    class PopupManager
    {
        List<PopupWindow> activeWindows;

        public PopupManager()
        {
            activeWindows = new List<PopupWindow>();
        }

        public PopupWindow Create(string title, string text, int timeout = 5000)
        {
            PopupWindow win = new PopupWindow();
            win.Title = title;
            win.Text = text;

            win.PopupHidden += new EventHandler(win_PopupHidden);

            activeWindows.Add(win);
            updatePositions();

            win.Show();

            return win;
        }

        void win_PopupHidden(object sender, EventArgs e)
        {
            PopupWindow win = (PopupWindow)sender;

            win.PopupHidden -= win_PopupHidden;

            int start = activeWindows.IndexOf(win);

            activeWindows.Remove(win);

            updatePositions();
        }

        void updatePositions()
        {
            Console.WriteLine("Updating positions");

            if (activeWindows.Count == 0)
            {
                return;
            }

            Rectangle workingArea = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;

            for (int i = activeWindows.Count - 1; i >= 0; i -= 1)
            {
                PopupWindow activePopup = activeWindows[i];

                if (i == activeWindows.Count - 1)
                {
                    Console.WriteLine("IsLast");
                    activePopup.SetDesktopLocation(workingArea.Width - activePopup.Width, workingArea.Height - activePopup.Height);
                }
                else
                {
                    Console.WriteLine("Is {0}", i);
                    PopupWindow prev = activeWindows[i + 1];
                    activePopup.SetDesktopLocation(workingArea.Width - activePopup.Width, prev.DesktopLocation.Y - activePopup.Height);
                }
            }
        }
    }
}
