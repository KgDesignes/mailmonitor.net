﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Timer = System.Timers.Timer;

namespace MailMonitor
{
    public partial class PopupWindow : Form
    {
        protected enum Status { Hidden, Showing, Idling, Hiding, Selected }

        public string Title { 
            get {
                return lblTitle.Text;
            }
            internal set {
                lblTitle.Text = value;
            }
        }
        
        new public string Text { get {
            return lblText.Text;
        }
            internal set {
                lblText.Text = value;
                this.Height = lblText.Top + lblText.Height + 12;
            }
        }

        public bool MouseEnabled { get; set; }

        private const int Timeout = 5; //Segundos
        private const int AnimationTime = 1; //Segundos
        private const int AnimationSteps = AnimationTime * 30;
        
        private Timer timeoutTimer;
        private Timer animationTimer;
        private int currentAnimationStep;
        private Status status;

        public PopupWindow()
        {
            InitializeComponent();

            KeyPreview = true;

            status = Status.Hidden;

            AllowTransparency = true;

            timeoutTimer = new Timer();
            //timeoutTimer.SynchronizingObject = this;
            timeoutTimer.Interval = Timeout * 1000;

            timeoutTimer.Tick +=new EventHandler(timeoutTimer_Elapsed);
            
            animationTimer = new Timer();
            //animationTimer.SynchronizingObject = this;
            animationTimer.Interval = (AnimationTime * 1000 / AnimationSteps);

            animationTimer.Tick += new EventHandler(animationTimer_Elapsed);

            MouseEnabled = true;

            MouseEnter += new EventHandler(PopupWindow_MouseEnter);
            MouseLeave += new EventHandler(PopupWindow_MouseLeave);

            foreach ( Control c in Controls ) {
                c.Click += new EventHandler(bubblingOnClick);
                c.MouseClick += new MouseEventHandler(bubblingOnMouseClick);
            }
        }

        ~PopupWindow()
        {
            Console.WriteLine("Liberando PopupWindow");
        }

        #region Mouse Control

        private void bubblingOnClick(object sender, EventArgs args)
        {
            OnClick(args);
        }

        private void bubblingOnMouseClick(object sender, MouseEventArgs args)
        {
            OnMouseClick(args);
        }

        void PopupWindow_MouseEnter(object sender, EventArgs e)
        {
            if (MouseEnabled)
            {
                EndShow();
                timeoutTimer.Stop();
                status = Status.Selected;
            }
        }

        void PopupWindow_MouseLeave(object sender, EventArgs e)
        {
            if (MouseEnabled)
            {
                BeginIdle();
            }
        }

        #endregion

        #region Idle

        void BeginIdle()
        {
            status = Status.Idling;

            timeoutTimer.Tick += timeoutTimer_Elapsed;

            timeoutTimer.Start();
        }

        void EndIdle()
        {
            timeoutTimer.Tick -= timeoutTimer_Elapsed;
        }

        void timeoutTimer_Elapsed(object sender, EventArgs e)
        {
            timeoutTimer.Stop();
            EndIdle();
            BeginHide();
        }

        #endregion

        void BeginShow()
        {
            animationTimer.Stop();

            //animationTimer.Tick += new EventHandler(animationTimer_Elapsed);

            Opacity = 0;
            status = Status.Showing;
            base.Show();

            OnPopupShowing(new EventArgs());

            currentAnimationStep = 1;

            animationTimer.Start();
        }

        void EndShow()
        {
            animationTimer.Stop();
            Opacity = 1;
        }

        void BeginHide()
        {
            animationTimer.Stop();

            //animationTimer.Tick += new EventHandler(animationTimer_Elapsed);

            Opacity = 1;
            status = Status.Hiding;
            base.Show();

            OnPopupHiding(new EventArgs());

            currentAnimationStep = 1;

            animationTimer.Start();
        }

        void EndHide()
        {
            animationTimer.Stop();

            Opacity = 0;

            //MouseEnter -= PopupWindow_MouseEnter;

            //MouseLeave -= PopupWindow_MouseLeave;

            base.Hide();
        }

        #region Events

        public event EventHandler PopupShowing;

        private void OnPopupShowing(EventArgs args)
        {
            if (PopupShowing!= null)
            {
                PopupShowing(this, args);
            }
        }

        public event EventHandler PopupShown;

        private void OnPopupShown(EventArgs args)
        {
            if (PopupShown != null)
            {
                PopupShown(this, args);
            }
        }

        public event EventHandler PopupHiding;

        private void OnPopupHiding(EventArgs args)
        {
            if (PopupHiding != null)
            {
                PopupHiding(this, args);
            }
        }

        public event EventHandler PopupHidden;

        private void OnPopupHidden(EventArgs args)
        {
            if (PopupHidden != null)
            {
                PopupHidden(this, args);
            }
        }

        #endregion

        new public void Show()
        {
            BeginShow();           
        }

        public void Hide(bool force = false)
        {
            if (force)
            {
                MouseEnabled = false;
                Enabled = false;
            }

            BeginHide();
        }

        void animationTimer_Elapsed(object sender, EventArgs e)
        {
            //Console.WriteLine("OnShownTick {0}/{1}", currentAnimationStep, AnimationSteps);

            if (currentAnimationStep < AnimationSteps)
            {
                if (status == Status.Showing)
                {
                    Opacity = ((double)currentAnimationStep / AnimationSteps);
                }
                else if (status == Status.Hiding)
                {
                    Opacity = 1 - ((double)currentAnimationStep / AnimationSteps);
                }

                currentAnimationStep += 1;
                //animationTimer.Start();
            }
            else
            {
                if (status == Status.Showing)
                {
                    OnPopupShown(new EventArgs());
                    EndShow();
                    BeginIdle();
                }
                else if (status == Status.Hiding)
                {
                    OnPopupHidden(new EventArgs());
                    EndHide();
                }
            }
        }
    }
}
