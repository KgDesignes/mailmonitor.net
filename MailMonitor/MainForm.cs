﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MailMonitor.Popup;
using System.Runtime.InteropServices;

namespace MailMonitor
{
    public partial class MainForm : Form
    {
        private bool forceClose = false;

        private AccountManager accountManager;
        private Dictionary<Guid, ImapMonitor> monitorList;
        private PopupManager popupManager;
        
        public MainForm()
        {
            InitializeComponent();

            popupManager = new PopupManager();

            DesktopLocation = ((Point)Properties.Settings.Default.WindowPosition);

            recoverWindow();
        }

        void accountManager_AccountDeleted(object sender, AccountEventArgs args)
        {
            this.StopMonitoringAccount(args.UID);
        }

        void accountManager_AccountUpdated(object sender, AccountEventArgs args)
        {
            this.RestarMonitoringAccount(args.UID, args.Account);
        }

        private void RestarMonitoringAccount(Guid uid, Account account)
        {
            if (this.monitorList.ContainsKey(uid))
            {
                ImapMonitor monitor = this.monitorList[uid];
                monitor.Restart();
            }
        }

        void accountManager_AccountAdded(object sender, AccountEventArgs args)
        {
            this.StartMonitoringAccount(args.UID, args.Account);
        }

        public void StopMonitoringAccount(Guid uid)
        {
            if (this.monitorList.ContainsKey(uid))
            {
                ImapMonitor monitor = this.monitorList[uid];
                monitor.Stop();
                this.monitorList.Remove(uid);
                this.lvAccounts.Items.RemoveByKey(uid.ToString());
            }
        }

        public void StartMonitoringAccount(Guid uid, Account account)
        {
            ImapMonitor tmpMonitor = new ImapMonitor(account);

            tmpMonitor.ConnectError += new EventHandler<ConnectionEventArgs>(tmpMonitor_ConnectError);
            tmpMonitor.ConnectSuccess += new EventHandler<ConnectionEventArgs>(tmpMonitor_ConnectSuccess);

            tmpMonitor.MessageCountUpdate += new EventHandler<MessageMonitorEventArgs>(tmpMonitor_MessageCountUpdate);
            tmpMonitor.NewMessage += new EventHandler<MessageMonitorEventArgs>(imapMonitor_NewMessage);

            this.monitorList.Add(uid, tmpMonitor);

            ListViewItem lvItem = new AccountListViewItem(uid, account);
            lvItem.SubItems.Add("-");
            lvItem.SubItems.Add("-");
            lvItem.SubItems.Add("-");
            lvItem.SubItems.Add("Connecting");
            lvAccounts.Items.Add(lvItem);

            tmpMonitor.Start();
        }

        void tmpMonitor_MessageCountUpdate(object sender, MessageMonitorEventArgs e)
        {
            ImapMonitor monitor = (ImapMonitor)sender;
            this.updateMessageCount(monitor.Account.UID, monitor);
        }

        void tmpMonitor_MessageDeleted(object sender, MessageMonitorEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void tmpMonitor_ConnectSuccess(object myObject, ConnectionEventArgs myArgs)
        {
            ImapMonitor monitor = (ImapMonitor)myObject;
            
            PopupWindow popup = popupManager.Create(monitor.Account.Name, String.Format("You have {0} unread messages", monitor.UnreadMessageCount), 5000);
            popup.Click += new EventHandler(monitor.OnPopupClick);

            this.updateMessageCount(monitor.Account.UID, monitor);
        }

        delegate void UpdateMessageCount(Guid uid, ImapMonitor monitor);

        void updateMessageCount(Guid uid, ImapMonitor monitor)
        {
            if (InvokeRequired)
            {
                Invoke(new UpdateMessageCount(updateMessageCount), new object[] { uid, monitor });
            }
            else
            {
                AccountListViewItem lvi = (AccountListViewItem)lvAccounts.Items[uid.ToString()];

                lvi.SubItems[1].Text = monitor.MessageCount.ToString();
                lvi.SubItems[2].Text = monitor.UnreadMessageCount.ToString();
                lvi.SubItems[3].Text = "Connected";
            }
        }

        void tmpMonitor_ConnectError(object myObject, ConnectionEventArgs myArgs)
        {
            ImapMonitor monitor = (ImapMonitor)myObject;
            popupManager.Create("Error", String.Format("No se pudo conectar a la cuenta {0}", monitor.Account.Name), 5000);

            AccountListViewItem lvi = (AccountListViewItem)lvAccounts.Items[monitor.Account.UID.ToString()];

            lvi.SubItems[4].Text = "Connection Error";
        }

        public void InitializeMonitorList()
        {
            this.monitorList = new Dictionary<Guid, ImapMonitor>();

            foreach (Guid uid in accountManager.Accounts.Keys)
            {
                this.StartMonitoringAccount(uid, accountManager.Accounts[uid]);
            }
        }

        void imapMonitor_NewMessage(object myObject, MessageMonitorEventArgs myArgs)
        {
            ImapMonitor monitor = (ImapMonitor)myObject;

            string[] messagePreview = new string[myArgs.NewMessages.Length];

            for (int i = 0; i < myArgs.NewMessages.Length; i += 1)
            {
                messagePreview[i] =
                    myArgs.NewMessages[i].From + " (" + myArgs.NewMessages[i].Date.ToShortDateString() + " " + myArgs.NewMessages[i].Date.ToShortTimeString() + ")\n" +
                    myArgs.NewMessages[i].Subject;
            }

            PopupWindow newMessagePopup = popupManager.Create(monitor.Account.Name, String.Format("You have {0} unread messages\nYou received {1} new messages\n\n{2}", monitor.UnreadMessageCount, myArgs.NewMessages.Length, string.Join("\n", messagePreview)), 5000);
            
            newMessagePopup.Click += new EventHandler(monitor.OnPopupClick);

            this.updateMessageCount(monitor.Account.UID, monitor);
        }

        private void notifyIconToolStripMenuItemClose_Click(object sender, EventArgs e)
        {
            this.forceClose = true;
            this.Close();
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.RestoreFromTray();
        }

        private void buttonAddAccount_Click(object sender, EventArgs e)
        {
            this.accountManager.AddAccount();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowPosition();

            if (e.CloseReason == CloseReason.UserClosing && ! this.forceClose)
            {
                e.Cancel = true;
                //this.ShowInTaskbar = false;
                this.MinimizeToTray();
            }
        }

        private void accountListContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (lvAccounts.SelectedItems.Count <= 0)
            {
                e.Cancel = true;
            }
        }

        private void editAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountListViewItem currentItem = (AccountListViewItem)lvAccounts.SelectedItems[0];
            accountManager.EditAccount(currentItem.UID);
        }

        private void runCommandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountListViewItem currentItem = (AccountListViewItem)lvAccounts.SelectedItems[0];

            this.monitorList[currentItem.UID].RunCommand();
        }

        private void deleteAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountListViewItem currentItem = (AccountListViewItem)lvAccounts.SelectedItems[0];
            accountManager.DeleteAccount(currentItem.UID);
        }

        private void lvAccounts_DoubleClick(object sender, EventArgs e)
        {
            AccountListViewItem item = (AccountListViewItem)lvAccounts.SelectedItems[0];
            this.monitorList[item.UID].RunCommand();
        }

        private void MinimizeToTray()
        {
            this.WindowState = FormWindowState.Minimized;
            this.Hide();
        }

        [DllImport("user32.dll")]
        private static extern
            bool SetForegroundWindow(IntPtr hWnd);


        private void RestoreFromTray()
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

            SetForegroundWindow(Handle);
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            saveWindowPosition();

            if (this.WindowState == FormWindowState.Minimized)
            {
                this.MinimizeToTray();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            PopupWindow startupPopup = popupManager.Create("MailMonitor.NET", "Iniciando.");

            startupPopup.Click += new EventHandler(startupPopup_Click);

            accountManager = new AccountManager();
            accountManager.LoadSettings();

            InitializeMonitorList();

            accountManager.AccountAdded += new AccountManager.AccountChangeHandler(accountManager_AccountAdded);
            accountManager.AccountUpdated += new AccountManager.AccountChangeHandler(accountManager_AccountUpdated);
            accountManager.AccountDeleted += new AccountManager.AccountChangeHandler(accountManager_AccountDeleted);
        }

        void startupPopup_Click(object sender, EventArgs e)
        {
            PopupWindow startupPopup = ((PopupWindow)sender);

            startupPopup.Click -= startupPopup_Click;

            startupPopup.MouseEnabled = false;
            startupPopup.Hide();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (accountManager.Accounts.Count > 0)
            {
                this.MinimizeToTray();
            }
        }

        private void updateNowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountListViewItem currentItem = (AccountListViewItem)lvAccounts.SelectedItems[0];

            this.monitorList[currentItem.UID].Update();
        }

        private void recoverWindow() {
            if (DesktopLocation.X < 0 || DesktopLocation.Y < 0)
            {
                StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void saveWindowPosition() {
            if (DesktopLocation.X >= 0 && DesktopLocation.Y >= 0)
            {
                if ((DesktopLocation.X != Properties.Settings.Default.WindowPosition.X) || (DesktopLocation.Y != Properties.Settings.Default.WindowPosition.Y))
                {
                    Properties.Settings.Default.WindowPosition = DesktopLocation;
                    Properties.Settings.Default.Save();
                }
            }
        }
    }
}
