﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AE.Net.Mail;

namespace MailMonitor
{
    class MessageMonitorEventArgs:EventArgs
    {
        public MailMessage[] NewMessages { get; protected set; }

        public MessageMonitorEventArgs()
        {
        }

        public MessageMonitorEventArgs(MailMessage[] newMessages)
        {
            // TODO: Complete member initialization
            NewMessages = newMessages;
        }
    }
}
