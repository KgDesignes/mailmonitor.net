﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Diagnostics;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;

using AE.Net.Mail;
using AE.Net.Mail.Imap;

using Timer = System.Timers.Timer;

namespace MailMonitor
{
    class ImapMonitor
    {
        private ImapClient ImapClient { get; set; }

        private Timer IdleTimer { get; set; }
        private Timer CheckTimer { get; set; }

        private string NextUID { get; set; }
       
        public int MessageCount { get; protected set;  }
        public int UnreadMessageCount { get; protected set; }

        public Account Account { get; private set; }

        Process process;

        #region Events

        public event EventHandler<ConnectionEventArgs> ConnectSuccess;

        private void OnConnectSuccess(ConnectionEventArgs args)
        {
            if (ConnectSuccess != null)
            {
                ConnectSuccess(this, args);
            }
        }

        public event EventHandler<ConnectionEventArgs> ConnectError;

        private void OnConnectError(ConnectionEventArgs args)
        {
            if (ConnectError != null)
            {
                ConnectError(this, args);
            }
        }

        public event EventHandler<MessageMonitorEventArgs> MessageCountUpdate;

        private void OnMessageCountUpdate(MessageMonitorEventArgs args)
        {
            if (MessageCountUpdate != null)
            {
                MessageCountUpdate(this, args);
            }
        }

        public event EventHandler<MessageMonitorEventArgs> NewMessage;

        private void OnNewMessage(MessageMonitorEventArgs args)
        {
            if (NewMessage != null)
            {
                NewMessage(this, args);
            }
        }

        #endregion

        public ImapMonitor(Account account)
        {
            Account = account;

            ImapClient = new ImapClient();

            IdleTimer = new Timer();
            IdleTimer.Elapsed += new ElapsedEventHandler(idleTimer_Elapsed);

            CheckTimer = new Timer();
            CheckTimer.Elapsed += new ElapsedEventHandler(checkTimer_Elapsed);
        }        

        private void InitMonitor()
        {
            try
            {
                ImapClient.Connect(Account.Host, Account.Port, true, false);

                ImapClient.Login(Account.Username, Account.Password);

                if (ConnectSuccess != null)
                {
                    ConnectSuccess(this, new ConnectionEventArgs());
                }

                this.updateRecentMessages();

                if (this.Account.CheckInterval < 30)
                {
                    IdleTimer.Interval = 30 * 60 * 1000;
                    IdleTimer.Start();

                    CheckTimer.Interval = this.Account.CheckInterval * 60 * 1000;
                    CheckTimer.Start();
                }
            }
            catch ( Exception ex)
            {
                OnConnectError(new ConnectionEventArgs());
            }
        }

        void checkTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("UPDATING AT " + DateTime.Now.ToLongDateString());
            IdleTimer.Stop();

            updateRecentMessages();

            IdleTimer.Start();
        }

        void idleTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("NOOPING AT " + DateTime.Now.ToLongDateString());
            ImapClient.Noop();

            IdleTimer.Start();
        }

        private void EndMonitor()
        {
            try
            {
                IdleTimer.Stop();
                CheckTimer.Stop();
                ImapClient.Disconnect();
            }
            catch (Exception ex)
            {
                OnConnectError(new ConnectionEventArgs());
            }
            finally
            {
                
            }
        }

        public void Start()
        {
            InitMonitor();
        }

        public void Stop()
        {
            EndMonitor();
        }

        public void updateRecentMessages()
        {
            try
            {
                Mailbox Inbox = ImapClient.Examine("INBOX");
                
                int GoogleUnseen = 0;

                if (Account.Host == "imap.gmail.com")
                {
                    Lazy<MailMessage>[] foundMessages = ImapClient.SearchMessages(SearchCondition.Unseen(), true);
                    GoogleUnseen = foundMessages.Length;
                }

                MailMessage[] newMessages = null;
                bool newMail = false;

                if ( ! String.IsNullOrEmpty(NextUID) && (Inbox.NextUID != NextUID) ) {
                    newMail = true;
                }

                if (newMail)
                {
                    newMessages = ImapClient.GetMessages(NextUID, Inbox.NextUID, true, false);
                }

                NextUID = Inbox.NextUID;
                MessageCount = Inbox.NumMsg;
                UnreadMessageCount = Inbox.NumUnSeen > 0 ? Inbox.NumUnSeen : GoogleUnseen;

                OnMessageCountUpdate(new MessageMonitorEventArgs());

                if (newMail)
                {
                    OnNewMessage(new MessageMonitorEventArgs(newMessages));
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        internal void Restart()
        {
            this.EndMonitor();
            this.InitMonitor();
        }

        public void Update()
        {
            CheckTimer.Stop();
            updateRecentMessages();
            CheckTimer.Start();
        }

        [DllImport("user32.dll")]
        private static extern
            bool SetForegroundWindow(IntPtr hWnd);

        public void RunCommand()
        {
            try
            {
                if (process == null || process.HasExited)
                {
                    Console.WriteLine("Starting process");
                    ProcessStartInfo psi = new ProcessStartInfo(Account.Command, Account.CommandArgs);

                    process = Process.Start(psi);
                }
                else
                {
                    Console.WriteLine("Recycling process");
                    SetForegroundWindow(process.MainWindowHandle);
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error ejecutando el comando: " + ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        public void OnPopupClick(object sender, EventArgs args)
        {
            RunCommand();
            ((PopupWindow)sender).Hide(true);
        }
    }
}
