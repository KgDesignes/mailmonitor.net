﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace MailMonitor.Security
{
    class EncryptionHelper
    {
        byte[] privateBytes;

        string privateKey;

        string PrivateKey {
            get { return this.privateKey; }
            set { 
                this.privateKey = value;
                this.clearPrivateBytes();
            }
        }

        public EncryptionHelper(string privateKey)
        {
            this.privateKey = privateKey;
        }

        private void clearPrivateBytes()
        {
            this.privateBytes = null;
        }

        private byte[] getPrivateBytes()
        {
            if (this.privateBytes == null)
            {
                if (String.IsNullOrEmpty(privateKey))
                {
                    return new byte[] { };
                }

                this.privateBytes = UnicodeEncoding.Unicode.GetBytes(this.privateKey);
            }

            return this.privateBytes;
        }

        public static string getHash(string textData)
        {
            MD5 md5 = MD5.Create();
            byte[] md5UnencryptedBytes = UnicodeEncoding.Unicode.GetBytes(textData);
            byte[] md5EncryptedBytes = md5.ComputeHash(md5UnencryptedBytes);

            byte[] saltedUnencryptedBytes = new byte[md5UnencryptedBytes.Length + md5EncryptedBytes.Length];
            md5EncryptedBytes.CopyTo(saltedUnencryptedBytes, 0);
            md5UnencryptedBytes.CopyTo(saltedUnencryptedBytes, md5EncryptedBytes.Length);

            SHA512 sha = SHA512.Create();
            byte[] encryptedBytes = sha.ComputeHash(saltedUnencryptedBytes);

            return ByteToHexString(encryptedBytes);
        }


        public string Encrypt(string textData) {
            byte[] privateBytes = this.getPrivateBytes();

            byte[] unencryptedBytes = UnicodeEncoding.Unicode.GetBytes(textData);

            byte[] encryptedBytes = ProtectedData.Protect(unencryptedBytes, privateBytes, DataProtectionScope.CurrentUser);

            return ByteToHexString(encryptedBytes);
        }

        public string Decrypt(string hash)
        {
            byte[] privateBytes = this.getPrivateBytes();
            byte[] encryptedBytes = HexStringToByte(hash);

            try
            {
                byte[] unencryptedBytes = ProtectedData.Unprotect(encryptedBytes, privateBytes, DataProtectionScope.CurrentUser);

                return UnicodeEncoding.Unicode.GetString(unencryptedBytes);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private static string ByteToHexString(byte[] bytes)
        {
            string hexString = "";
            foreach (byte b in bytes)
            {
                string hexChar = Convert.ToString(b, 16);
                hexChar = (hexChar.Length < 2 ? "0" : "") + hexChar;
                hexString += hexChar;
            }
            return hexString;
        }

        private static byte[] HexStringToByte(string hexString)
        {
            byte[] byteArray = new byte[hexString.Length / 2];

            for (int i = 0; i < byteArray.Length; i += 1)
            {
                string sub = hexString.Substring(i * 2, 2);
                byteArray[i] = Convert.ToByte(sub, 16);
            }

            return byteArray;
        }
    }
}
